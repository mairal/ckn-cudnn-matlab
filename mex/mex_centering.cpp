#include <mexutils.h>
#include <common.h>

template <typename T>
inline void callFunction(mxArray* plhs[], const mxArray*prhs[],const int nrhs) {
   Matrix<T> X;
   getMatrix<T>(prhs[0],X);
   const int nchannels = nrhs == 1 ? 1 : static_cast<int>(mxGetScalar(prhs[1]));
   centering(X,nchannels);
}

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
   if (nrhs != 1 && nrhs != 2)
      mexErrMsgTxt("Bad number of inputs arguments");

   if (nlhs != 0)
      mexErrMsgTxt("Bad number of output arguments");

   if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS) {
      callFunction<double>(plhs,prhs,nrhs);
   } else {
      callFunction<float>(plhs,prhs,nrhs);
   }
}

