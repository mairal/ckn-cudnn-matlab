#include <mexutils.h>
#include <kmeans.h>

template <typename T>
inline void callFunction(mxArray* plhs[], const mxArray*prhs[],const int nrhs) {
   Matrix<T> X;
   getMatrix<T>(prhs[0],X);
   const int d = getScalarStruct<int>(prhs[1],"d");
   int threads = getScalarStructDef<int>(prhs[1],"threads",-1);
   const int num_iter = getScalarStructDef<int>(prhs[1],"num_iter",10);
   if (threads == -1) {
      threads=1;
#ifdef _OPENMP
      threads =  MIN(MAX_THREADS,omp_get_num_procs());
#endif
   } 
   threads=init_omp(threads);
//   mkl_set_num_threads(threads);
   plhs[0]=createMatrix<T>(X.m(),d);
   Matrix<T> Z;
   getMatrix<T>(plhs[0],Z);
   fast_kmeans(X,Z,num_iter);
}

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
   if (nrhs != 2)
      mexErrMsgTxt("Bad number of inputs arguments");

   if (nlhs != 1)
      mexErrMsgTxt("Bad number of output arguments");

   if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS) {
      callFunction<double>(plhs,prhs,nrhs);
   } else {
      callFunction<float>(plhs,prhs,nrhs);
   }
}

