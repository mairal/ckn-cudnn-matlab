#include <mexutils.h>

template <typename T>
inline void callFunction(mxArray* plhs[], const mxArray*prhs[],const int nrhs) {
   Matrix<T> X;
   getMatrix<T>(prhs[0],X);
   vmlSetMode(0x00000003 | 0x00280000 | 0x00000100);
   plhs[0]=createMatrix<T>(X.m(),X.n());
   Matrix<T> Z;
   getMatrix<T>(plhs[0],Z);
   vExp(Z.m()*Z.n(),X.rawX(),Z.rawX());
}



void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
   if (nrhs != 1)
      mexErrMsgTxt("Bad number of inputs arguments");

   if (nlhs != 1)
      mexErrMsgTxt("Bad number of output arguments");

   if (mxGetClassID(prhs[0]) == mxDOUBLE_CLASS) {
      callFunction<double>(plhs,prhs,nrhs);
   } else {
      callFunction<float>(plhs,prhs,nrhs);
   }
}

