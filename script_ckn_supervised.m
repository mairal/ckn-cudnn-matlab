device='gpu';
threads=4;
zero_pad=1;  % careful zero_padding is incompatible with even patch sizes
centering=0;
whitening=0;
type_learning_init=8;  % fast, cheap initialization
dataset='cifar-10_w';  % pre-centered-and-whitened dataset
lambda2=0.01;

% ************************* with no data augmentation ********
% produces about 90.5% accuracy on cifar, with single model and no data augmentation.
data_augment=0;
npatches=   [3 1 3 1 3 1 3 1 3 1 3 1 3 1];  
subsampling=[1 1 1 2 1 1 1 2 1 1 1 2 1 2];
nfilters=[256 128 256 128 256 256 256 256 256 256 256 256 256 256];   % the "128" in the filter size are here only to gain a x4 speed-up without losing accuracy
type_kernel=[0 1 0 1 0 1 0 1 0 1 0 1 0 1]
sigmas=[0.5]; 
lambda=0.1;
alt_optim=0;
it_eval=1; % evaluate validation or test every it_eval epochs (cost some time); mostly for monitoring purposes
init_rate=10;
ndecrease_rate=100;
nepochs=105;
train_ckn_supervised_gpu(npatches,subsampling,nfilters,sigmas,type_kernel,zero_pad,centering,whitening,type_learning_init,lambda,lambda2,alt_optim,it_eval,init_rate,ndecrease_rate,nepochs,data_augment,device,threads,dataset);

% ************************* with basic data augmentation ******************
% does about 92%
%data_augment=1;
% npatches=   [3 1 3 1 3 1 3 1 3 1 3 1 3 1 3 1];  
% subsampling=[1 1 1 2 1 1 1 2 1 1 1 2 1 2 1 2];
% nfilters=[384 384 384 384 384 384 384 384 384 384 384 384 384 384];   
% type_kernel=[0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1]
% nepochs=110;
% train_ckn_supervised_gpu(npatches,subsampling,nfilters,sigmas,type_kernel,zero_pad,centering,whitening,type_learning_init,lambda,lambda2,alt_optim,it_eval,init_rate,ndecrease_rate,nepochs,data_augment,device,threads,dataset);
